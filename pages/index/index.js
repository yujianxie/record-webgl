// index.js
// 获取应用实例
import { webgl } from "./webgl.js";
const app = getApp();
Page({
  data: {
    videoUrl: "",
    isStart: false,
  },
  canvas: null,
  onReady() {
    const selector = wx.createSelectorQuery();
    selector
      .select("#webgl")
      .node((res) => {
        const canvas = res.node;
        this.recorder = wx.createMediaRecorder(canvas, {
          videoBitsPerSecond: 600
        });

        const camera = wx.createCameraContext();
        let isFirst = true;
        let render = null;
        let i = 1;

        const listener = camera.onCameraFrame((frame) => {
          // console.log(frame.width, 1111122);
          if (isFirst && frame.width) {
            const gl = canvas.getContext("webgl");
            render = webgl(gl, frame.width, frame.height);
            isFirst = false;
          }
          const data = new Uint8Array(frame.data);
          render(data);
        });

        listener.start({
          success: () => {
            // setTimeout(() => {
            //   listener.stop();
            // }, 5000);
          },
        });
      })
      .exec();
  },
  startRecord() {
    this.recorder.start().then(() => {
      this.setData({
        isStart: true,
      });
    });
  },
  stopRecord() {
    this.recorder.stop().then((res) => {
      console.log("stopRecord result: ", res);
      this.setData({
        isStart: false,
        videoUrl: res.tempFilePath,
      });
    });
  },
});
